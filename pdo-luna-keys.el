;;;
;;; Using unicode characters in a FreeBSD vt keymap file to generate
;;; 2-byte UTF-8 sequences that are interpreted in Emacs as keypress
;;; sequences.
;;;
;;; Luckily these sequences are obscure enough not to be used by any
;;; mode I know of, so I use them to enable modifier-applied cursor
;;; keys, etc, in a FreeBSD console/terminal.
;;;
;;; Codepoint   UTF-8   Emacs
;;;    1C0      C7 80   M-g C-M-@
;;;    1C1      C7 81   M-g C-M-a
;;;    1C2      C7 82   M-g C-M-b
;;;    1C3      C7 83   M-g C-M-c
;;;    1C4      C7 84   M-g C-M-d
;;;    1C5      C7 85   M-g C-M-e
;;;    1C6      C7 86   M-g C-M-f
;;;    1C7      C7 87   M-g C-M-g
;;;    1C8      C7 88   M-g C-M-h
;;;    1C9      C7 89   M-g C-M-i
;;;    1CA      C7 8A   M-g C-M-j
;;;    1CB      C7 8B   M-g C-M-k
;;;    1CC      C7 8C   M-g C-M-l
;;;    1CD      C7 8D   M-g C-M-m
;;;    1CE      C7 8E   M-g C-M-n
;;;    1CF      C7 8F   M-g C-M-o
;;;    1D0      C7 90   M-g C-M-p
;;;
(define-key function-key-map (kbd "M-g C-M-@") (kbd "<left>"))
(define-key function-key-map (kbd "M-g C-M-a") (kbd "<right>"))
(define-key function-key-map (kbd "M-g C-M-b") (kbd "<up>"))
(define-key function-key-map (kbd "M-g C-M-c") (kbd "<down>"))

(define-key function-key-map (kbd "M-g C-M-d") (kbd "S-<left>"))
(define-key function-key-map (kbd "M-g C-M-e") (kbd "S-<right>"))
(define-key function-key-map (kbd "M-g C-M-f") (kbd "S-<up>"))
(define-key function-key-map (kbd "M-g C-M-g") (kbd "S-<down>"))

(define-key function-key-map (kbd "M-g C-M-h") (kbd "C-<left>"))
(define-key function-key-map (kbd "M-g C-M-i") (kbd "C-<right>"))
(define-key function-key-map (kbd "M-g C-M-j") (kbd "C-<up>"))
(define-key function-key-map (kbd "M-g C-M-k") (kbd "C-<down>"))

(define-key function-key-map (kbd "M-g C-M-l") (kbd "S-C-<left>"))
(define-key function-key-map (kbd "M-g C-M-m") (kbd "S-C-<right>"))
(define-key function-key-map (kbd "M-g C-M-n") (kbd "S-C-<up>"))
(define-key function-key-map (kbd "M-g C-M-o") (kbd "S-C-<down>"))

(define-key function-key-map (kbd "M-g C-M-p") (kbd "S-<tab>"))
